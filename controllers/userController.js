const User = require('../models/user')
const bcrypt = require('bcrypt')
const auth = require('../auth')
const Course = require('../models/course')
/*
	npm i bcrypt : install command for bcrypt. 
	this is use for hashing passwords so that other users or developers would not see the password input.
	
	10 : number of times it would be hash. 

	Syntax: 
	bcrypt.hashSync(<stringToBeHashed>, <saltRounds>)
	
	npm i jsonwebtoken : this is for authentication for the login route
	
	B3n3v0l3nc3@
*/
exports.checkEmailExists = ({email}) => {
	return User.find({email: email}).then(result => {
		if(result.length > 0) return true
		else return false
	})
}

exports.registerUser = ({...rest}) => {
	let newUser = new User({
		firstName: rest.firstName, 
		lastName: rest.lastName,
		email: rest.email,
		password: bcrypt.hashSync(rest.password, 10),
		mobileNo: rest.mobileNo
	})

	return newUser.save().then((user, err) => {
		if(err) return false 
		else return true 
	})
}

exports.loginUser = ({...rest}) => {
	return User.findOne({email: rest.email}).then( result => {
/*		console.log(result[0].password)
		console.log(rest.password)*/
		if(result == null) return false
			else{
				const isPasswordCorrect = bcrypt.compareSync(rest.password, result.password)
				if(isPasswordCorrect) return { access: auth.createAccessToken(result) }
					else return false
			}
	})
}

exports.getProfile = ({...rest}) => {
	return User.findById({_id: rest.id}).then(result => {
		result.password = ""
		return result
	})
}

//Async await be used in enrolling the user because we will need to update 2 separate documents when enrolling a user.

//Using the await keyword will allow the enroll method to complete updating the user before returning a response back  
exports.enroll = async(data) => {
	let isUserUpdated = await User.findById(data.userId).then( user => {
		user.enrollments.push({courseId: data.courseId})

		return user.save().then((user, error) => {
			if(error) return false
			else return true	
		})
	})
//Using the await keyword will allow the enroll method to complete the course before returning a response back.
	let isCourseUpdated = await Course.findById(data.courseId).then(course => {
		course.enrollees.push({userId: data.userId});

		return course.save().then((course, err) => {
			if(err) return false
			else return true
		})
	})

	if(isUserUpdated && isCourseUpdated) return true 
	else return false
}
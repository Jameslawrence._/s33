const Course = require('../models/course')
const auth = require('../auth')

exports.addCourse = ({...course}) => {
	let newCourse = new Course({
		name: course.name, 
		description: course.description,
		price: course.price 
	})

	return newCourse.save().then((course, err) => {

		if(err) return false
		else return course
	})			

}

exports.getAllCourses = () => {
	return Course.find({}).then( result => {return result})
}

exports.activeCourses = () => {
	return Course.find({isActive:true}).then(result => {return result})
}

exports.getCourse = ({...course}) => {
	return Course.findById({_id:course.courseId}).then(result => { return result }) 
}

exports.updateCourse = (change, {courseId}) => {
	return Course.findById({_id:courseId}).then( result => {
		if(result === null) return false
		else{	
			Object.assign(result, change).save()
			return true
		}
	})
}

exports.archiveCourse = ({courseId}) => {
		return Course.find({_id: {$ne: courseId}}).then( result => {
			return result
		})
}
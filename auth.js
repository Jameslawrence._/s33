const jwt = require('jsonwebtoken')
const secret = "CourseBookingAPI"

exports.createAccessToken = (user) => {
	//The data will be received from the registration form
	//When the user logs in, a token will be created with user's information
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin,
	}
	//Generate a JSON web token using the jwt's sign method
	//Generates the token using the form data, and the secret code with no additional options.
	return jwt.sign(data, secret, {})
}

exports.verify = (req, res, next) => {
	//the token is retrived from the request header
	let token = req.headers.authorization
	//token received and not undefined
	if(typeof token !== "undefined"){
		console.log(token)
		//the token sent is a type of bearer token which received contains the 
		//bearer as a prefix to the string 
		//Syntax: string.slice(start, end)
		//7 because BEARER_ number of characters plus space.
		token = token.slice(7, token.length)
		return jwt.verify(token, secret, (err, data) => {
			if(err) return res.send({auth: "failed"})
			else next()
		})
	}else return res.send({auth: "failed"})
}

exports.decode = (token) => {
	if(typeof token !== "undefined"){
		token = token.slice(7, token.length)
		return jwt.verify(token, secret, (err, data) => {
			if(err) return null
			else{
				return jwt.decode(token, {complete: true}).payload
			} 


		})
	}else return null
}




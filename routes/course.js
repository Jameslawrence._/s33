const express = require('express')
const courseController = require('../controllers/courseController')
const auth = require('../auth')
const router = express.Router()

router.post('/', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	console.log(userData)
	if(userData.isAdmin === true){
	courseController.addCourse(req.body).then( resultFromController => {
		res.send(resultFromController)
	})}else res.send(false)
})

router.get('/All', (req, res) => {
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController))
})

router.get('/activecourse', (req, res) => {
	courseController.activeCourses().then(resultFromController => res.send(resultFromController))
})

router.get('/:courseId', (req, res) => {
	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController))
})

router.patch('/:courseId', auth.verify ,(req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin === true){
	courseController.updateCourse(req.body, req.params).then(resultFromController => res.send(resultFromController))
	}else res.send(false)
})

router.delete('/:courseId', auth.verify, (req, res) => {
	courseController.archiveCourse(req.params).then(resultFromController => res.send(resultFromController))
})
module.exports = router
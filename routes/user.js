const express = require('express')
const auth = require('../auth')
const userController = require('../controllers/userController')
const router = express.Router()



router.post('/checkEmail', (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => {
		res.send(resultFromController)
	})
})

router.post('/register', (req, res) => {
	userController.registerUser(req.body).then(resultFromController => {
		res.send(resultFromController)
	}) 
})

router.post('/login', (req, res) => {
	userController.loginUser(req.body).then(resultFromController => {
		res.send(resultFromController)
	})
})

router.get('/details', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	console.log(userData)
	userController.getProfile(req.body).then(resultFromController => res.send(resultFromController))
})		

//for enrolling a user
router.post('/enroll', auth.verify, (req, res) => {
	let data = {
		userId: req.body.userId,
		courseId: req.body.courseId
	}
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin === false){ 	
	userController.enroll(data).then(resultFromController => res.send(resultFromController))
	}else return res.send(false)
})

module.exports = router